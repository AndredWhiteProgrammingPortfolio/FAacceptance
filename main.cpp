//
//  Andre White
//  Awhiteis21@csu.fullerton.edu
//
//  
//
//  Created by Andre White on 2/28/16.
//  Copyright © 2016 AndreWhite. All rights reserved.
//

#include <iostream>
#include <string.h>
#include <stack>
using namespace std;
int states[5][3]={{0, 1, 2},{1, 1, 3},{2, 4, 2},{3, 4, 4},{4, 4, 4}};                           //state table
bool parseInput(string input)                                                                   //function will parse through input and return true if accepted and false if not
{
    char curChar;                                                                               //current character
    int state = 0;                                                                              //current state
    for(int x=0;x<input.length();x++){
        curChar=input[x];
        switch (curChar) {                                                                      //switch state based on input
            case 'a':
                state=states[state][1];
                break;
            case 'b':
                state=states[state][2];
                break;
            default:
                state=4;
                break;
        }
    }
    if (state==2||state==3) {                                                                   //if final state is any of the accepting states then return true, else return false
        return true;
    }
    else return false;
        
}
char prompt(){
    char response;                                                                              //user response(Y/N)
    string output;                                                                              //program output
    string input;                                                                               //user input
    printf("%s", "Enter a string:");                                                            //prompt user for string
    cin>>input;                                                                                 //get string
    bool accepted=parseInput(input);                                                            //parse input
    if (accepted) {
        output="Yes";
    }
    else
        output="No";
    printf("%s %s \n","Accepted:",output.c_str());                                              //output if accepted
    printf("%s", "Continue?(y/n)");
    cin>>response;
    return response;
}

int main(int argc, char** argv){
    while(prompt()=='y'){                                                                       //program loop
    }
    return 0;
}
